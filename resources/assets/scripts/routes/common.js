import Masonry from 'masonry-layout';
import SimpleLightbox from 'simplelightbox';
import AOS from 'aos';

export default {
  init() {
    // JavaScript to be fired on all pages

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    console.log('hello common')
    
    setupLoadBlogPopup()
    addSpintoNav()
    getwindowSize ()
    getMobilewindowSize()
    closeModalImpressum()
    closeModalDatenschutz()
    closeHompageModal()
    toggleNavOpacity()
    closeModalImpressumPopup()
    closeModalDatenschutzPopup()
    // hideBtnOnscroll()
    // hideFooterBtnOnscroll()
    // hidePopupBtnOnscroll()
   


    // _________ Librairies set up_______________
    // Masonry
    setTimeout(() => {
      var elem = document.querySelector('.grid');
      var msnry = new Masonry( elem, {
      // options
      itemSelector: '.grid-item',
      stamp: '.stamp',
      columnWidth: '.grid-sizer',
      horizontalOrder: true,
      });
  
      console.log('msnr---->', msnry); 

      // AOS (scroll animation homepage) 
      $(function() {
        AOS.init();
      });
    }, 500);

    // Lightbox 
    function setupLightbox () {
      var lightbox = new SimpleLightbox('.gallery a', { 
        overlay: true,
        nav: false,
        close: true,
        closeText: '',
        swipeClose: false,
        docClose: false,
        // animationSlide: false,
        // fadeSpeed: 100,
        /* options */ });
        console.log(lightbox);
    }
    
    // Get window size to set up lightbox only in mobile version
    function getwindowSize () {
      var w = window.innerWidth;
      console.log('windowsize---->', w);
      if (w <= 768) {
        setupLightbox()
      }
    }



    function getMobilewindowSize () {
      var w = window.innerWidth;
      console.log('windowsize2---->', w)
      if (w <= 576) {
        hideBtnOnscroll()
        hideFooterBtnOnscroll()
        hidePopupBtnOnscroll()
      }
    }
   
    window.initWindowSize = function () {
      var w = window.innerWidth;
      console.log('windowsize---->', w)
      if (w <= 768) {
        setupLightbox()
      }
    }

    // _________ Modals opening, closing and animations_______________
    // Close footer modals
    function closeModalImpressum () {
      $('#impressum button').on('click', function () {
        $('#impressum').addClass('hide-modal')
        setTimeout(function(){
          $('#impressum').removeClass('hide-modal')
          $('#impressum').modal('hide')
        }, 1000);
      })
    }

    function closeModalDatenschutz () {
      $('#datenschutz button').on('click', function () {
        $('#datenschutz').addClass('hide-modal')
        setTimeout(function(){
          $('#datenschutz').removeClass('hide-modal')
          $('#datenschutz').modal('hide')
        }, 1000);
      })
    }
    // duplicate for blog-popup 
    function closeModalImpressumPopup () {
      $('#impressum-modal button').on('click', function () {
        $('#impressum-modal').addClass('hide-modal')
        setTimeout(function(){
          $('#impressum-modal').removeClass('hide-modal')
          $('#impressum-modal').modal('hide')
        }, 1000);
      })
    }

    function closeModalDatenschutzPopup () {
      $('#datenschutz-modal  button').on('click', function () {
        $('#datenschutz-modal ').addClass('hide-modal')
        setTimeout(function(){
          $('#datenschutz-modal ').removeClass('hide-modal')
          $('#datenschutz-modal ').modal('hide')
        }, 1000);
      })
    }

    // Fadeout animation on homepage modal
    function closeHompageModal () {
      $('#blog-popup .modal-btn').on('click', function(){
        console.log('close modal')

        $('#blog-popup .modal-dialog').addClass('opacity');
        $('.modal-backdrop').css('opacity', '0')

        setTimeout(function(){
          $('#blog-popup').modal('hide');
        }, 700);
      });
    }

    function repositionModalBtn () {
      setTimeout(function(){
        $('#blog-popup .modal-btn').css('display', 'flex')
      }, 1000);
      console.log('reposition btn')
    }

    // _________ Navbar_______________

    // FadeIn fadeOut animation navbar
    function toggleNavOpacity () {
      $('.nav-btn').on('click', function () {
        $('nav').toggleClass('add-opacity')
      })
    }

    // Navbar button spin 
    function addSpintoNav () {
      $('.nav-btn').on('click', function () {
        console.log('click');
        $('.nav-btn').toggleClass('is-active');
        $('nav').toggleClass('hide-nav');
      })
    }
    let fadeInTimer;
    // Hide homepage button when scrolling (for client, about, contact pages)
    function hideBtnOnscroll () {
      
      window.onscroll = function() {
        $('.page-clients-data .homepage-btn').hide();
        $('.page-about-data .homepage-btn').hide();
        $('.page-contact-data .homepage-btn').hide();
        clearTimeout(fadeInTimer);
        fadeInTimer = setTimeout(function(){
        $('.page-clients-data .homepage-btn').show();
        $('.page-about-data .homepage-btn').show();
        $('.page-contact-data .homepage-btn').show();
        },200);
      };
    }

    // Hide homepage button when scrolling (on blog popup)
    function hidePopupBtnOnscroll () {
      document.querySelector('#blog-popup').onscroll = function () { 
        
          $('.modal-btn').hide();
      
        clearTimeout(fadeInTimer);
         fadeInTimer = setTimeout(function(){
          $('.modal-btn').show();
        },200);
      }
    }

    // Hide homepage button when scrolling (in impressum and datenschutz modals)
    function hideFooterBtnOnscroll () {
      $('.modal').each(function () {
        $(this).find('.modal-content').on('scroll', function () { 
          console.log('this', $(this))
          $(this).find('.scroll-btn').hide();
          clearTimeout(fadeInTimer);
          fadeInTimer = setTimeout(function (){
            $('.scroll-btn').show();
          },200);
        })
      })
    }

  
    // _________ Ajax call Homepage
    // Pop up blog ajax set up
    function setupLoadBlogPopup() {   
      $('.grid-item').on('click', function () {
       
          // get id from clicked post
          let postId = parseInt($(this).attr('data-postid'));
          console.log('id--->', postId)
          // call Ajax Call function with postID as parameter
          loadPopup(postId);
          $('.modal-btn').addClass('is-active')
          $('#blog-popup .modal-dialog').removeClass('opacity');
          $('#blog-popup .modal-btn').css('display', 'none')
        })
       }
      
      //  AJAX Call
      function loadPopup(postId) {
        // console.log('load called')
        // Ajax call made on url, start action
       $.ajax({
         url: `//${location.host}/wp-admin/admin-ajax.php`,
         data: {
           action: 'loadBlogPopup',
           postId: postId,
         },
         type: 'POST',
         dataType: 'html',
        //  after data retrieved, display them in html
         success: function (data) {
          //  console.log('data', data)
           $('#blog-popup .modal-data').html(data);
           console.log('data')
          //  Open modal after data loaded for nicer visual effect
           $('#blog-popup').modal('show');
           
           setTimeout(function(){
            $('.modal-backdrop.show ').css('opacity', '1')
          }, 1000);


           repositionModalBtn()
         },
       });
      }
  },
};
