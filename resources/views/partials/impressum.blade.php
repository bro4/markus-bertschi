@php
    $impressum = get_post(678);
    $datenschutz = get_post(1372)
@endphp

<div class="modal" id="impressum" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="scroll-btn button-light hamburger is-active hamburger--spin" aria-label="Close">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
      <div class="impressum-page">
        <h2 class="pb-8">{{$impressum->post_title}}</h2>
        <p>{!! apply_filters('the_content', $impressum->post_content) !!}</p>
      </div>
    </div>
  </div>
</div>

  <div class="modal" id="datenschutz" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-margin-el"></div>
          <button type="button" class="scroll-btn button-light hamburger is-active hamburger--spin" >
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
          <div class="datenschutz-page">
            <h2 class="pb-4 pb-sm-8">{{$datenschutz->post_title}}</h2>
            <div>
              <p>{!! apply_filters('the_content', $datenschutz->post_content) !!}</p>
            </div>
        </div>
      </div>
    </div>
  </div>


{{-- Modals for #blog pop-up modal --}}


<div class="modal" id="impressum-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="scroll-btn button-light hamburger is-active hamburger--spin" aria-label="Close">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
      <div class="impressum-page">
        <h2 class="pb-8">{{$impressum->post_title}}</h2>
        <p>{!! apply_filters('the_content', $impressum->post_content) !!}</p>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="datenschutz-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-margin-el"></div>
      <button type="button" class="scroll-btn button-light hamburger is-active hamburger--spin" >
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
      </button>
      <div class="datenschutz-page">
        <h2 class="pb-4 pb-sm-8">{{$datenschutz->post_title}}</h2>
        <div>
          <p>{!! apply_filters('the_content', $datenschutz->post_content) !!}</p>
        </div>
      </div>
    </div>
  </div>
</div>