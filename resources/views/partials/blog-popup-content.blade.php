@php
$fotos = @get_field('post_fotos');
$mypost_id = @get_queried_object()->ID;
$footer = @get_post(1298);
$email = @get_field('email', 1298);
$beschreibung = @get_field('titel');

@endphp

<section id="blog-popup-header" class="mt-lg-12">
  <header>
    <div></div>
    <a href="/">
      <h1>Fotografie</h1>
      <h2>Markus<br>Bertschi</h2>
    </a>
  </header>
</section>

<div class="foto-container gallery">
  @foreach ($fotos as $el)
  <div class="image">
    <div class="{{$el['grosse_1']}} foto1">
      @if ($el['post_foto_1'])
      <a href="{{$el['post_foto_1']}}"><img src="{{$el['post_foto_1']}}" alt=""></a>
      @else
      <div></div>
      @endif
    </div>
    @if ($el['post_foto_2'])
    <div class="{{$el['grosse_2']}}">
      <a href="{{$el['post_foto_2']}}"><img src="{{$el['post_foto_2']}}" alt=""></a>
    </div>
    @else
    @endif
  </div>
  @endforeach
</div>

<footer class="desktop-footer d-flex flex-column">
  <div class="bildlegende">
    <h2>{!!the_content()!!}</h2>
    @foreach ($beschreibung as $el)
    <div class="d-flex ">
      <p class="bildlegende-titel pr-2">{{$el['titel']}}:</p>
      <p>{!!$el['inhalt']!!}</p>
    </div>
    @endforeach
  </div>
</footer>

<script>
  window.initWindowSize()
</script>