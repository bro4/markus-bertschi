@php
$kontaktPost = get_post(10);
 $kontaktDaten = get_field('kontakt');
//  var_dump($kontaktDaten);
@endphp

<section id="contact">
    <header>
        <div></div>
        <a href="/">
            <h1>Fotografie</h1>
            <h2>Markus<br>Bertschi</h2>
        </a>
    </header>
    @foreach ($kontaktDaten as $el)
    {{-- {{var_dump($el)}} --}}
        <article>
            <div class="d-md-flex mb-3">
                <div class="contact-left  flex-column mr-2">
                    <div class="kontaktdaten text-section pb-lg-8">
                        <h2 class="pt-2 pb-lg-5">{!! $el['arbeitsfeld'] !!}</h2>
                        <h2 class="adresse pt-2 pb-8"><a href="{!! $el['maps_link']!!}" target="_blank">{!! $el['adresse'] !!}</a></h2>
                    </div>
                    <div class="kontaktdaten daten text-section">
                        @if ($el['telefonnummer'])
                        <a href="tel:{{$el['telefonnummer']}}"><h2>{!! $el['telefonnummer'] !!}</h2></a>
                        @endif
                        @if ($el['email'])
                        <a href="mailto:{{$el['email']}}"><h2>{!! $el['email'] !!}</h2></a>
                        @endif
                        @if ($el['webseite'])
                        <a href="https://{{$el['webseite']}}" target="_blank"><h2>{!! $el['webseite'] !!}</h2></a>
                        @endif
                    </div>
                </div>
                <div class="contact-img">
                    @if ($el['bild'])
                    <img src="{!! $el['bild'] !!}">
                    @else 
                    <iframe src="{!!$el['video']!!}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    @endif
                </div>
            </div>
        </article>
    @endforeach
</section>


    
