@php
 $kunden = get_post(11);
 $fotos = get_field('kunden_bilder');
//  var_dump($fotos);
@endphp
<section class="clients">
  <div class="content">

    {{-- Column left --}}
      <div class="column-fotos">
        @foreach($fotos as $el) 
          <div @if($el['mobil'] == true) class="fotos" @else class="fotos mobile-hide" @endif>
            <img src="{!!$el['kunden_bild']!!}">
          </div>
        @endforeach
        <header class="header-ipad">
          <a href="/">
            <h1>Fotografie</h1>
            <h2>Markus<br>Bertschi</h2>
          </a>
        </header>
      </div>

      {{-- Colum center --}}
      <div class="text-container d-sm-flex">
        <div class="column left text-section">
          <header class="clients-header">
            <a href="/">
              <h1>Fotografie</h1>
              <h2>Markus<br>Bertschi</h2>
            </a>
          </header>
          <h2 class="clients-title">{{get_the_title()}}</h2>
          <h3>Firmen</h3>
          <p>{!!get_field('firmen')!!}</p>
          <h3>Medien</h3>
          <p>{!!get_field('medien')!!}</p>
        </div>

        {{-- Column right --}}
        <div class="column right text-section">
          <h3>Agenturen</h3>
          <p>{!!get_field('agenturen')!!}</p> 
          <h3>Award</h3>
          <p>{!!get_field('awards')!!}</p> 
          <h3>Engagment</h3>
          <p>{!!get_field('engagement')!!}</p>
        </div>
    </div>

  </div>
</section>
