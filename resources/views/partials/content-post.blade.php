@if(get_the_ID()===1320)
<div class="grid-item stamp-logo post">
    <header>
        <a href="/">
            <h1 class="header-padding">Fotografie</h1>
            <h2 class="header-padding">Markus<br>Bertschi</h2>
        </a>
    </header>
</div>
@elseif(get_the_ID()===1450)
<div class="grid-item margin" data-postid="{{get_the_ID()}}" data-aos="fade-up">
    {!!get_the_post_thumbnail()!!}
    <h1 class="category-title ">{!! get_the_title()!!}</h1>
</div>
@else
<div class="grid-item" data-postid="{{get_the_ID()}}" data-aos="fade-up">
    {!!get_the_post_thumbnail()!!}
    <h1 class="category-title">{!! get_the_title()!!}</h1>
</div>
@endif