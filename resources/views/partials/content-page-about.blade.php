@php
 $about = get_post(6);
 $fotos = get_field('bilder');
// var_dump($fotos);
@endphp
<section class="about">
  <div class="content">

    {{-- Column left --}}
    <div class= "column-fotos">
      @foreach($fotos as $el) 
        <div @if($el['mobil'] == true) class="fotos" @else class="fotos mobile-hide" @endif>
          <img src="{!!$el['uber_bild']!!}">
        </div>
      @endforeach
      <header class="header-ipad">
        <a href="/">
            <h1>Fotografie</h1>
            <h2>Markus<br>Bertschi</h2>
        </a>
      </header>
    </div>

    {{-- Column center --}}
    <div class="text-container d-sm-flex">
      <div class="column left text-section">
        <header class="clients-header">
          <a href="/">
            <h1>Fotografie</h1>
            <h2>Markus<br>Bertschi</h2>
          </a>
        </header>
        <h2 class="clients-title">{{get_the_title()}}</h2>
        <p>{!!get_the_content()!!}</p>
      </div>

      {{-- Column right --}}
    <div class="column right text-section">
      @if (get_field('bild_links'))
      <div class="fotos fotos-links">
        <img src={{get_field('bild_links')}}>
        </div>
      @else 
      <p class="about-margin">{{get_field('text_links')}}</p>
      @endif
    </div>
    
  </div>
</section>


