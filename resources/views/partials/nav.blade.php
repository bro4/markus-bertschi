
  <nav class="hide-nav">
    <a href="/" @if(is_home()) class="hide" @else class="back "@endif)>
      <div class="">
        <img src="@asset("images/arrow-back.svg")" alt="">
      </div>
    </a>
      <div class="">
        <a href={{get_the_permalink(11)}}><h2>Kunden</h2></a>
        <a href={{get_the_permalink(6)}}><h2>Über</h2></a>
        <a href={{get_the_permalink(10)}}><h2>Kontakt</h2></a>
    </div>
  </nav>

  <button class="button-light nav-position nav-btn hamburger hamburger--spin" type="button">
    <span class="hamburger-box">
      <span class="hamburger-inner"></span>
    </span>
  </button>


  @include('partials.homepage-btn')

