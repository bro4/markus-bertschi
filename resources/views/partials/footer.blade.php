@php
 $mypost_id = @get_queried_object()->ID;
 $footer = @get_post(1298);
 $email = @get_field('email', 1298);
//  var_dump($email);
@endphp

@include('partials.impressum')

<footer class="desktop-footer d-flex flex-column">
  @if ( @$mypost_id != 10 )
  <div class="mobile-hide">
   <p> {!! apply_filters('the_content', $footer->post_content) !!} </p>
  </div>
  {{-- @else --}}
  @endif
    <div class="impressum">
      <div class="mobile-footer justify-content-center text-center">
        <a href="tel:{{get_field('telefon', 1298)}}" class="pr-3 text-right"><h2>Telefon</h2></a>
        <a href="mailto:{{get_field('email', 1298)}}" class="pl-3 text-left"><h2>Email</h2></a>
      </div>
      <div class="d-flex mb-6 mt-4 justify-content-center">
        <p class="pr-3 impressum-btn text-right" data-toggle="modal" data-target="#impressum"> Impressum</p>
        <p class="pl-3 text-left" data-toggle="modal" data-target="#datenschutz"> Datenschutz</p>
      </div>
    </div>
</footer> 
