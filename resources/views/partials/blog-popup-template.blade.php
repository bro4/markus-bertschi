
<div id="blog-popup" class="modal animate__animated animate__fadeIn" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button class="scroll-btn modal-btn button-light nav-position hamburger hamburger--spin"  aria-label="Close">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
      <div class="modal-data">
        {{-- data from ajax call --}}
      </div>  
    </div>
  </div>
</div>