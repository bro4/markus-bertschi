<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
      <main class="contain">
        @include('partials.nav')
        @yield('content')
        @include('partials.blog-popup-template')
      </main> 
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>

</html>
