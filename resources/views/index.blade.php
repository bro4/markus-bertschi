@extends('layouts.app')

@section('content')
@include('partials.page-header')

@if (!have_posts())
<div class="alert alert-warning">
  {{ __('Sorry, no results were found.', 'sage') }}
</div>
{!! get_search_form(false) !!}
@endif

<div class="grid">
  <div class="grid-sizer"></div>
  @php
  $currentPost = 1;
  $allPostCount = wp_count_posts()->publish;
  @endphp
  @while (have_posts()) @php the_post() @endphp
  @include('partials.content-'.get_post_type())
  @if($currentPost==3 || $currentPost == 5 || $currentPost == 9)
  <div class="grid-item" data-postid="{{get_the_ID()}}" style="pointer-events: none;height:1px;" data-aos="fade-up">
  </div>
  @endif
  @php
  $currentPost++;
  @endphp
  @endwhile

</div>
{!! get_the_posts_navigation() !!}
@endsection

{{-- Something to commit --}}